<?php

namespace Nitra\PromoBundle\Controller;

use Nitra\StoreBundle\Controller\NitraController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PromoController extends NitraController
{
    /**
     * @Template("NitraPromoBundle:Promo:index.html.twig")
     *
     * @param int $limit
     *
     * @return array Template context
     */
    public function promoAction($limit = 3)
    {
        $promos = $this->getDocumentManager()->createQueryBuilder('NitraPromoBundle:Promo')
            ->sort('sortOrder')
            ->field('image')
                ->exists(true)
                ->notEqual('')
            ->limit($limit)
            ->getQuery()->execute();

        return array(
            'promos' => $promos,
        );
    }
}