# PromoBundle

## Описание
Данный бандл предназначен для работы (вывода, обработки) с:

* рекламой (Promo) вывод реклам
## PromoController
* promoAction - получает объекты класса Promo для текущего магазина

## Подключение
Для подключения данного модуля в проект необходимо:

* composer.json:

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-site-promobundle": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

//...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\PromoBundle\NitraPromoBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* app/config/routing.yml:

```yaml
#...
    nitra_promo:
        resource: "@NitraPromoBundle/Resources/config/routing.yml"
        prefix:   /
#...
```